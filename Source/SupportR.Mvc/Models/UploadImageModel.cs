﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Collections.Generic;
using System.Web;

namespace SupportR.Mvc.Models
{
    public class UploadImageModel
    {
        [Required]
        public HttpPostedFileBase File { get; set; }
    }
}