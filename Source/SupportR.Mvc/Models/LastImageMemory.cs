﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Collections.Generic;

namespace SupportR.Mvc.Models
{
    public class LastImageMemory
    {
        private static readonly LastImageMemory _instance = new LastImageMemory();
        private ConcurrentDictionary<string, string> _lastImages = new ConcurrentDictionary<string, string>();

        public static LastImageMemory Instance
        {
            get { return _instance; }
        }

        private LastImageMemory()
        {
        }

        public string GetLastImage(string groupName)
        {
            string imageUrl;

            if (_lastImages.TryGetValue(groupName, out imageUrl))
            {
                return imageUrl;
            }

            return null;
        }

        public void SetLastImage(string groupName, string imageUrl)
        {
            _lastImages.AddOrUpdate(groupName, imageUrl, (s1, s2) => imageUrl);
        }
    }
}