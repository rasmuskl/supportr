﻿using System;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using SignalR;
using SupportR.Mvc.Hubs;
using SupportR.Mvc.Models;

namespace SupportR.Mvc.Controllers
{
    public class HomeController : Controller
    {
        private static readonly char[] Chars = "abcdefghijklmnopqrstuvwxyz".ToCharArray();

        public ActionResult Index(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return RedirectToAction("Index", new { id = GenerateSupportKey() });
            }

            dynamic model = new ExpandoObject();

            model.Id = id;

            return View(model);
        }

        public ActionResult UploadImage(string id)
        {
            return View(new UploadImageModel());
        }

        [HttpPost]
        public ActionResult UploadImage(string id, UploadImageModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var guid = Guid.NewGuid();

            var extension = Path.GetExtension(model.File.FileName);

            if (!new [] { ".png", ".gif", ".jpg" }.Contains(extension.ToLowerInvariant()))
            {
                ModelState.AddModelError("File", "Only PNG, GIF and JPG is supported.");
                return View(model);
            }

            if (model.File.ContentLength > 500000)
            {
                ModelState.AddModelError("File", "Only images up to 500kb are allowed.");
                return View(model);
            }

            string relativePath = Path.Combine("Images", guid + extension);
            var filePath = Server.MapPath("~/" + relativePath);
            model.File.SaveAs(filePath);

            var hubContext = GlobalHost.ConnectionManager.GetHubContext<SupportHub>();
            hubContext.Clients[id].updateImage("/" + relativePath);

            LastImageMemory.Instance.SetLastImage(id, "/" + relativePath);

            return RedirectToAction("UploadImage", new { id = id });
        }

        private string GenerateSupportKey()
        {
            var random = new Random();

            var key = string.Empty;

            for (var i = 0; i < 6; i++)
            {
                key += Chars[random.Next(Chars.Length)];
            }

            return key;
        }
    }
}