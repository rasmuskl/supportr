﻿using System;
using System.Linq;
using System.Collections.Generic;
using SignalR.Hubs;
using SupportR.Mvc.Models;

namespace SupportR.Mvc.Hubs
{
    [HubName("support")]
    public class SupportHub : Hub
    {
        public void Init(string groupName)
        {
            Groups.Add(Context.ConnectionId, groupName);

            var lastImageUrl = LastImageMemory.Instance.GetLastImage(groupName);

            if (!string.IsNullOrWhiteSpace(lastImageUrl))
            {
                Caller.updateImage(lastImageUrl);
            }
        }
    }
}